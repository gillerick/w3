# W3

A simple Flutter to showcase Flutter's rich widgets and layouts.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Demos
![Page 1](assets/demo_images/page_1.jpeg)

Page 1 - Splash Screen

![Page 2](assets/demo_images/page_2.jpeg)

Page 2 - Forms Widget

![Page 3](assets/demo_images/page_3.jpeg)

Page 3 - Grid View

## Key Learning Areas
- Grid View
- List View
- TextField
- FormField 
- TextFormField
- Navigating to different pages 


