import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Stacks extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text("Cohort Stacks"),
      ),
      body: Container(
        child: GridView.count(
          primary: false,
          padding: const EdgeInsets.all(20),
          crossAxisSpacing: 4,
          mainAxisSpacing: 4,
          crossAxisCount: 2,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.all(1),
              child: Column(
                children: [
                  Image.asset(
                    'assets/stack_images/go.png',
                    height: 120,
                    width: 160,
                  ),
                  Text(
                    "Go",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Web",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(1),
              child: Column(
                children: [
                  Image.asset(
                    'assets/stack_images/flutter.png',
                    height: 120,
                    width: 160,
                  ),
                  Text(
                    "Flutter",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Mobile",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(1),
              child: Column(
                children: [
                  Image.asset(
                    'assets/stack_images/mysql.png',
                    height: 120,
                    width: 160,
                  ),
                  Text(
                    "MySQL",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Databases",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(1),
              child: Column(
                children: [
                  Image.asset(
                    'assets/stack_images/vue.png',
                    height: 120,
                    width: 160,
                  ),
                  Text(
                    "Vue JS",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Web",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(1),
              child: Column(
                children: [
                  Image.asset(
                    'assets/stack_images/adonis.webp',
                    height: 120,
                    width: 160,
                  ),
                  Text(
                    "Adonis",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Web",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(1),
              child: Column(
                children: [
                  Image.asset(
                    'assets/stack_images/quarkus.png',
                    height: 110,
                  ),
                  Text(
                    "Quarkus",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Web",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
